package android.exercise.mini.calculator.app;
import android.media.audiofx.PresetReverb;

import java.util.*;

import java.io.Serializable;

public class SimpleCalculatorImpl implements SimpleCalculator {
  Deque<String> cur_state;
  boolean first_digit_add = false;
  public SimpleCalculatorImpl(){
    cur_state= new ArrayDeque<>();
    cur_state.add("0");


  }



  // todo: add fields as needed

  @Override
  public String output() {
    // todo: return output based on the current state
    StringBuilder string = new StringBuilder();
    for (String s : cur_state) {
      string.append(s);
    }
    return string.toString();
  }

  @Override
  public void insertDigit(int digit) {
    // todo: insert a digit
    String d = String.valueOf(digit);
    if (d.length()!=1){
      throw new RuntimeException("Illegal Digit");

    }
    if (!first_digit_add){
      first_digit_add=true;
      cur_state.remove();
    }

    cur_state.add(d);

  }

  @Override
  public void insertPlus() {
    // todo: insert a plus
    cur_state.add("+");

  }

  @Override
  public void insertMinus() {
    // todo: insert a minus
    cur_state.add("-");

  }

  @Override
  public void insertEquals() {
    // todo: calculate the equation. after calling `insertEquals()`, the output should be the result
    //  e.g. given input "14+3", calling `insertEquals()`, and calling `output()`, output should be "17"
    int res=0;
    String num = "0";
    String operator = "";
    boolean operator2after =false;
    while (!cur_state.isEmpty()) {
      String cur = cur_state.remove();
      if (cur.equals("+") || cur.equals("-")) {
        res += Integer.parseInt(num);
        if (operator2after){
          continue;
        }
        operator = cur;
        if (operator.equals("+")) {
          num = "0";
        } else {
          num = "-0";
        }
        operator2after =true;
      }
      else {
        num += cur;
        operator2after=false;

      }
    }
    res+= Integer.parseInt(num);
    String final1 = String.valueOf(res);
    for (int i = 0; i < final1.length(); i++){
      cur_state.add(Character.toString(final1.charAt(i)));
      //Process char
    }

  }

  @Override
  public void deleteLast() {
    // todo: delete the last input (digit, plus or minus)
    //  e.g.
    //  if input was "12+3" and called `deleteLast()`, then delete the "3"
    //  if input was "12+" and called `deleteLast()`, then delete the "+"
    //  if no input was given, then there is nothing to do here
    cur_state.removeLast();


  }

  @Override
  public void clear() {
    // todo: clear everything (same as no-input was never given)
    cur_state.clear();
    cur_state.add("0");
    first_digit_add=false;
  }

  @Override
  public Serializable saveState() {
    CalculatorState state = new CalculatorState();
    state.AddState(cur_state);

    // todo: insert all data to the state, so in the future we can load from this state
    return state;
  }

  @Override
  public void loadState(Serializable prevState) {
    if (!(prevState instanceof CalculatorState)) {
      return; // ignore
    }
    CalculatorState casted = (CalculatorState) prevState;
    cur_state= casted.getState();

    // todo: use the CalculatorState to load
  }

  private static class CalculatorState implements Serializable {
    /*
    TODO: add fields to this class that will store the calculator state
    all fields must only be from the types:
    - primitives (e.g. int, boolean, etc)
    - String
    - ArrayList<> where the type is a primitive or a String
    - HashMap<> where the types are primitives or a String
     */
    Deque<String> backup_state ;
    public void AddState(Deque<String> cur){
      backup_state= new ArrayDeque<>();
      backup_state.addAll(cur);

    }
    public Deque<String> getState(){
      return backup_state;
    }


  }
}
