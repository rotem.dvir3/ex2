package android.exercise.mini.calculator.app;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {


  @VisibleForTesting
  public SimpleCalculator calculator;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    if (calculator == null) {
      calculator = new SimpleCalculatorImpl();
    }
    /////find view //////////
    ArrayList<TextView> Numbuttons = new ArrayList<>();
    Numbuttons.add(findViewById(R.id.button0));
    Numbuttons.add(findViewById(R.id.button1));
    Numbuttons.add(findViewById(R.id.button2));
    Numbuttons.add(findViewById(R.id.button3));
    Numbuttons.add(findViewById(R.id.button4));
    Numbuttons.add(findViewById(R.id.button5));
    Numbuttons.add(findViewById(R.id.button6));
    Numbuttons.add(findViewById(R.id.button7));
    Numbuttons.add(findViewById(R.id.button8));
    Numbuttons.add(findViewById(R.id.button9));
    TextView EqualButt = findViewById(R.id.buttonEquals);
    TextView MinusButt= findViewById(R.id.buttonMinus);
    TextView PlusButt = findViewById(R.id.buttonPlus);
   TextView ClearButt = findViewById(R.id.buttonClear);
    TextView calcView = findViewById(R.id.textViewCalculatorOutput);
    View DelButton = findViewById(R.id.buttonBackSpace);


    /////****** change view ******\\\\\\\
    calcView.setText(calculator.output());

    //// set listeners ///////////////////////////////
    for(int i =0 ; i<10;i++){
      TextView curButton = Numbuttons.get(i);
      int finalI = i;

      curButton.setOnClickListener(v -> {
        try{
          calculator.insertDigit(finalI);
        }
        catch (Exception e) {
          e.printStackTrace();
        }
        calcView.setText(calculator.output());
      });
    }

    EqualButt.setOnClickListener(v -> {
      calculator.insertEquals();
      calcView.setText(calculator.output());
    });

    MinusButt.setOnClickListener(v -> {
      calculator.insertMinus();
      calcView.setText(calculator.output());
    });

    PlusButt.setOnClickListener(v -> {
      calculator.insertPlus();
      calcView.setText(calculator.output());
    });

    ClearButt.setOnClickListener(v -> {
      calculator.clear();
      calcView.setText(calculator.output());
    });

    DelButton.setOnClickListener(v->{
      calculator.deleteLast();
      calcView.setText(calculator.output());

    });

    /*
    TODO:
    - find all views
    - initial update main text-view based on calculator's output
    - set click listeners on all buttons to operate on the calculator and refresh main text-view
     */

  }

  @Override
  protected void onSaveInstanceState(@NonNull Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putSerializable("curState",calculator.saveState());


    // todo: save calculator state into the bundle
  }

  @Override
  protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
    super.onRestoreInstanceState(savedInstanceState);
      Serializable preState = savedInstanceState.getSerializable("curState");
      calculator.loadState(preState);
    // todo: restore calculator state from the bundle, refresh main text-view from calculator's output
  }
}